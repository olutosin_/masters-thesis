# Import all libraries required
import keras
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Convolution3D, MaxPooling3D
from sklearn.model_selection import train_test_split
from keras.utils import np_utils, generic_utils
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical
from keras.models import Sequential
from sklearn import model_selection
from sklearn import preprocessing
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import time
import cv2
import os

print("[INFO] starting application...")

# Load the datset
dataset = np.load("50-200-128-128.npy")
label = np.load("label-50-200-128-128.npy")

dataset_ = np.rollaxis(np.rollaxis(dataset, 3, 1), 3, 2)
data_ = dataset_

# Transform the labels
labels = np.ones((50, ), dtype = int)
labels = label


Train_data = [data_, labels]
X_train, Y_train =(Train_data[0], Train_data[1])
print("Shape of X_train is :", X_train.shape)

# Transforming 4D into 5D for CNN Model
Train_set = np.ones((50, 1, 128, 128, 200))
for h in range(50):
    Train_set[h][0][:][:][:] = X_train[h,:,:,:]

# Hot Encoding y_train
#Y_train = to_categorical(Y_train, 1)

# Developing the Model
model = Sequential()

# Conv Block1
model.add(Convolution3D(16, kernel_size = (3,3,3), strides = (1,1,1), padding = "same", data_format = "channels_first", activation = "relu"))
model.add(MaxPooling3D(pool_size = (2,2,2)))
model.add(BatchNormalization())
model.add(Dropout(0.3))

# Conv Block2
model.add(Convolution3D(32, kernel_size = (3,3,3), strides = (1,1,1), padding = "same", data_format = "channels_first", activation = "relu"))
model.add(MaxPooling3D(pool_size = (2,2,2)))
model.add(BatchNormalization())
model.add(Dropout(0.3))

# Conv Block3
model.add(Convolution3D(64, kernel_size = (3,3,3), strides = (1,1,1), padding = "same", data_format = "channels_first", activation = "relu"))
model.add(MaxPooling3D(pool_size = (2,2,2)))
model.add(BatchNormalization())
model.add(Dropout(0.3))

model.add(Flatten())

# FCC Blocks:
model.add(Dense(128, activation="relu"))

model.add(Dense(128, activation="relu"))

# Output Block
model.add(Dense(1, activation = "sigmoid"))

# Model Summary
print(model.summary())

# Compilation and training of the model
model.compile(optimizer = "adam", loss = "binary_crossentropy", metrics = ["accuracy"])


# Splitting dataset
X_train_new, X_val_new, y_train_new,y_val_new =  train_test_split(Train_set, Y_train, test_size=0.2, random_state=None)


# Training Model
print(format("[INFO] starting training...", "*^100"))
hist = model.fit(X_train_new, y_train_new, validation_data=(X_val_new,y_val_new),batch_size=5, nb_epoch = 25, shuffle=True)


start_of_training = datetime.now()
model.save("model2.h5")
#hist = model.fit(train_set, Y_train, batch_size=batch_size,
#         nb_epoch=nb_epoch,validation_split=0.2, show_accuracy=True,
#           shuffle=True)
end_of_training = datetime.now()

time_taken = end_of_training - start_of_training
print("Totatl training time is: ", time_taken)


# Model Evaluation
import pydot
import graphviz
score = model.evaluate(X_val_new, y_val_new)
print('Test score loss:', score[0])
print('Test score accuracy:', score[1])


# Plot history for loss and accuracy of the model
plt.figure()
plt.plot(hist.history['loss'])
plt.plot(hist.history['val_loss'])
plt.title('3D CNN model')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper right')
plt.show()


plt.plot(hist.history['accuracy'])
plt.plot(hist.history['val_accuracy'])
plt.title('3D CNN model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper right')
plt.show()

from keras.utils.vis_utils import plot_model
plot_model(model, to_file = "Model2.png")


print("[INFO] Application sucessfully executed")
