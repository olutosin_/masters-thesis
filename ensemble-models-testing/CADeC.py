# Ensemble 3DCNN[i...k] Inference Application
# Import all libraries required
import keras
from keras.models import load_model
import matplotlib.pyplot as plt
import pydicom as pydicom
import SimpleITK as sitk
from pathlib import Path
from glob import glob
import pandas as pd
import numpy as np
import argparse
import warnings
import imutils
import shutil
import glob
import math
import time
import cv2
import os


# Application message
print("[INFO] starting application...")

# Clearing console warnings
warnings.filterwarnings("ignore")

# Initialize aeguments parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--imagefile", required = "True",  
                help = "full path to the mhd/raw or dicom 3D CT scans folder")
ap.add_argument("-a", "--appmode", required = "True", type = str, default = "extended",
                help = "application mode specification: light or extended")
args = vars(ap.parse_args())


# Process passed arguments
imagefile_ = args["imagefile"]
imageformat = imagefile_.split(".")[-1]
appmode_ = args["appmode"]
appmode = appmode_.lower()

#print(imagefile_)
#print(imageformat)
#print(appmode)

# Load Ensemble Models (in HFD5 format)
print("[INFO] Loading ensemble models...")
CNN_3D_i = load_model("model1.h5")     # 50-250-128-128 
CNN_3D_j = load_model("model2.h5")     # 50-200-128-128
CNN_3D_k = load_model("model3.h5")     # 50-200-128-128
print("[INFO] ensemble models loaded successfully...")


# DICOM handling functions
# Load the slices
def load_scan(path):
    
    slices = [pydicom.read_file(path + "/" + s) for s in os.listdir(path) if s.endswith("dcm")]
    slices.sort(key = lambda x: int(x.InstanceNumber))
    
    try:
        slice_thickness = np.abs(slices[0].ImagePosition[2] - slices[1].ImagePosition[2])
    except:
        slice_thickness = np.abs(slices[0].SliceLocation - slices[1].SliceLocation)
    for s in slices:
        s.SliceThickness = slice_thickness
    
    # Raw slices
    return slices


# Extract arry from dicom slices
def get_pixels_hu(scans):
    image = np.stack([s.pixel_array for s in scans])
    # Converting to int16
    image = image.astype(np.int16)
    
    #print(image)
    #print(image == -2000)
    # Setting outside of the pxel to 1
    # Intercept is usually -1024 and air is 0
    image[image == -2000] = 0
    
    #print(image)
    
    # Converting to HU
    intercept = scans[0].RescaleIntercept
    slope = scans[0].RescaleSlope
    
    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)
        
    image += np.int16(intercept)
    image_ = np.array(image, dtype = np.int16)
    image_ = list(image_)
    return image_
 

# dicom hander
#data_path = "../LIDC-IDRI-0002/01-01-2000-98329/3000522-04919"
def dicom_handler():
    
    data_path = args["imagefile"]
    path = os.listdir(data_path)
    #print(len(path))

    patient = load_scan(data_path)
    #print(len(patient))
    slices3D = get_pixels_hu(patient)
    
    # list holding the 3D array
    return slices3D


# Image pre-processing into deep neural net format
# CNN_3D_i: (1 * 1 * 128 * 128 * 250)
# CNN_3D_j: (1 * 1 * 128 * 128 * 200)
# CNN_3D_k: (1 * 1 * 128 * 128 * 200)

def mhd_3D_i_preprocessor():

    #Load raw 3D scan
    length_slices_3D = []
    roll_axis3D = [] 
    new_slices3D = []
    slices_3D = []

    image3D = []
    for i in range(1):
        
        image = sitk.GetArrayFromImage(sitk.ReadImage(imagefile_))
        #print(image.shape)
        image3D.append(image)

    # Display CT scan: call slice viewer
    # Pre-process image to the format (1 * 1 * 128 * 128 * 250)
    w = 128
    h = 128
    
    # Loop through the 3D array of the CT scan
    for i, patients in enumerate(image3D):
        #print(patients.shape, i)
       
        new_slices = []
        
        for i, slice in enumerate(patients):
            if (i < 249) or (i == 249):
                img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
                #print("Data type: ", type(img_sm))
                new_slices.append(img_sm)
                #print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
            
            else:
                break
      
        slices_resized_3d = np.asarray(new_slices).astype('int16')
        #print("Data type of slices_resized_3d: ", type(slices_resized_3d))
        # Length of Main 3D
        length_slices_3D.append(len(slices_resized_3d))
        
        # Main 3D array
        new_slices3D.append(slices_resized_3d) 
        
        # append length of slices after each loop
        input = slices_resized_3d 
        #print("shape before rolling:", np.array(new_slices3D).shape)
        
        # Rolling
        ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
        roll_axis3D.append(ipt)
        #print(ipt.shape)
        #print("New slice 3D:", np.array(new_slices3D).shape)
        #print("New slice 3D rolled:", np.array(roll_axis3D).shape)
        
    return roll_axis3D


def mhd_3D_j_preprocessor():
    
    length_slices_3D = []
    roll_axis3D = [] 
    new_slices3D = []
    slices_3D = []

    #Load raw 3D scan
    image3D = []
    for i in range(1):
        
        image = sitk.GetArrayFromImage(sitk.ReadImage(imagefile_))
        #print(image.shape)
        image3D.append(image)

    # Display CT scan: call slice viewer
   
    # Pre-process image to the format (1 * 1 * 128 * 128 * 250)
    w = 128
    h = 128
    
    
    # Loop through the 3D array of the CT scan
    for i, patients in enumerate(image3D):
        #print(patients.shape, i)
       
        new_slices = []
        
        for i, slice in enumerate(patients):
            if (i < 199) or (i == 199):
                img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
                #print("Data type: ", type(img_sm))
                #print("Maximum pixel value: ",np.max(img_sm))
                new_slices.append(img_sm)
                #print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
            
            else:
                break
      
        slices_resized_3d = np.asarray(new_slices).astype('int16')
        #print("Data type of slices_resized_3d: ", type(slices_resized_3d))
        # Length of Main 3D
        length_slices_3D.append(len(slices_resized_3d))
        
        # Main 3D array
        new_slices3D.append(slices_resized_3d) 
        
        # append length of slices after each loop
        input = slices_resized_3d 
        #print("shape before rolling:", np.array(new_slices3D).shape)
        
        # Rolling
        ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
        roll_axis3D.append(ipt)
        #print(ipt.shape)
        #print("New slice 3D:", np.array(new_slices3D).shape)
        #print("New slice 3D rolled:", np.array(roll_axis3D).shape)
        
    return roll_axis3D

def mhd_3D_k_preprocessor():
    
    length_slices_3D = []
    roll_axis3D = [] 
    new_slices3D = []
    slices_3D = []

    #Load raw 3D scan
    image3D = []
    for i in range(1):
        
        image = sitk.GetArrayFromImage(sitk.ReadImage(imagefile_))
        #print(image.shape)
        image3D.append(image)

    # Display CT scan: call slice viewer
   
    # Pre-process image to the format (1 * 1 * 128 * 128 * 250)
    w = 128
    h = 128
    
    
    # Loop through the 3D array of the CT scan
    for i, patients in enumerate(image3D):
        #print(patients.shape, i)
       
        new_slices = []
        
        for i, slice in enumerate(patients):
            if (i < 199) or (i == 199):
                img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
                #print("Data type: ", type(img_sm))
                #print("Maximum pixel value: ",np.max(img_sm))
                new_slices.append(img_sm)
                #print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
            
            else:
                break
      
        slices_resized_3d = np.asarray(new_slices).astype('int16')
        #print("Data type of slices_resized_3d: ", type(slices_resized_3d))
        # Length of Main 3D
        length_slices_3D.append(len(slices_resized_3d))
        
        # Main 3D array
        new_slices3D.append(slices_resized_3d) 
        
        # append length of slices after each loop
        input = slices_resized_3d 
        #print("shape before rolling:", np.array(new_slices3D).shape)
        
        # Rolling
        ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
        roll_axis3D.append(ipt)
        #print(ipt.shape)
        #print("New slice 3D:", np.array(new_slices3D).shape)
        #print("New slice 3D rolled:", np.array(roll_axis3D).shape)
        
       
    return roll_axis3D

def dicom_3D_i_preprocessor():
    
    #Load raw 3D scan
    image3D = []
    length_slices_3D = []
    roll_axis3D = [] 
    new_slices3D = []
    
    image3D = dicom_handler()

    # Display CT scan: call slice viewer
   
    # Pre-process image to the format (1 * 1 * 128 * 128 * 250)
    w = 128
    h = 128
    
    # Loop through the 3D array of the CT scan
    for i, patients in enumerate(image3D):
        #print(patients.shape, i)
       
        new_slices = []
        
        for i, slice in enumerate(patients):
            if (i < 249) or (i == 249):
                img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
                #print("Data type: ", type(img_sm))
                #print("Maximum pixel value: ",np.max(img_sm))
                new_slices.append(img_sm)
                #print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
            
            else:
                break
      
        slices_resized_3d = np.asarray(new_slices).astype('int16')
        #print("Data type of slices_resized_3d: ", type(slices_resized_3d))
        # Length of Main 3D
        length_slices_3D.append(len(slices_resized_3d))
        
        # Main 3D array
        new_slices3D.append(slices_resized_3d) 
        
        # append length of slices after each loop
        input = slices_resized_3d 
        #print("shape before rolling:", np.array(new_slices3D).shape)
        
        # Rolling
        ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
        roll_axis3D.append(ipt)
        #print(ipt.shape)
        #print("New slice 3D:", np.array(new_slices3D).shape)
        #print("New slice 3D rolled:", np.array(roll_axis3D).shape)
        
    return roll_axis3D


def dicom_3D_j_preprocessor():
    
    length_slices_3D = []
    roll_axis3D = [] 
    new_slices3D = []
    slices_3D = []

    #Load raw 3D scan
    image3D = dicom_handler()
    
    # Display CT scan: call slice viewer
   
    # Pre-process image to the format (1 * 1 * 128 * 128 * 250)
    w = 128
    h = 128
    
    
    # Loop through the 3D array of the CT scan
    for i, patients in enumerate(image3D):
        #print(patients.shape, i)
       
        new_slices = []
        
        for i, slice in enumerate(patients):
            if (i < 199) or (i == 199):
                img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
                #print("Data type: ", type(img_sm))
                #print("Maximum pixel value: ",np.max(img_sm))
                new_slices.append(img_sm)
                #print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
            
            else:
                break
      
        slices_resized_3d = np.asarray(new_slices).astype('int16')
        #print("Data type of slices_resized_3d: ", type(slices_resized_3d))
        # Length of Main 3D
        length_slices_3D.append(len(slices_resized_3d))
        
        # Main 3D array
        new_slices3D.append(slices_resized_3d) 
        
        # append length of slices after each loop
        input = slices_resized_3d 
        #print("shape before rolling:", np.array(new_slices3D).shape)
        
        # Rolling
        ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
        roll_axis3D.append(ipt)
        #print(ipt.shape)
        #print("New slice 3D:", np.array(new_slices3D).shape)
        #print("New slice 3D rolled:", np.array(roll_axis3D).shape)
        
    return roll_axis3D


def dicom_3D_k_preprocessor():
    
    length_slices_3D = []
    roll_axis3D = [] 
    new_slices3D = []
    slices_3D = []

    #Load raw 3D scan
    image3D = dicom_handler()
    
    # Display CT scan: call slice viewer
   
    # Pre-process image to the format (1 * 1 * 128 * 128 * 250)
    w = 128
    h = 128
    
    
    # Loop through the 3D array of the CT scan
    for i, patients in enumerate(image3D):
        #print(patients.shape, i)
       
        new_slices = []
        
        for i, slice in enumerate(patients):
            if (i < 199) or (i == 199):
                img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
                #print("Data type: ", type(img_sm))
                #print("Maximum pixel value: ",np.max(img_sm))
                new_slices.append(img_sm)
                #print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
            
            else:
                break
      
        slices_resized_3d = np.asarray(new_slices).astype('int16')
        #print("Data type of slices_resized_3d: ", type(slices_resized_3d))
        # Length of Main 3D
        length_slices_3D.append(len(slices_resized_3d))
        
        # Main 3D array
        new_slices3D.append(slices_resized_3d) 
        
        # append length of slices after each loop
        input = slices_resized_3d 
        #print("shape before rolling:", np.array(new_slices3D).shape)
        
        # Rolling
        ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
        roll_axis3D.append(ipt)
        #print(ipt.shape)
        #print("New slice 3D:", np.array(new_slices3D).shape)
        #print("New slice 3D rolled:", np.array(roll_axis3D).shape)
        
         
    return roll_axis3D


# Sub-routines definition
# dicom light handler
def dicom_light_handler():
    
    # Save arrays processed in separate list : dicom
    dicom_3D_i_array = np.array(dicom_3D_i_preprocessor())
    dicom_3D_j_array = np.array(dicom_3D_j_preprocessor())
    dicom_3D_k_array = np.array(dicom_3D_k_preprocessor())

    # Transforming 4D into 5D for CNN Model
    input_array_i = np.ones((1, 1, 128, 128, 250))
    for h in range(1):
        input_array_i[h][0][:][:][:] = dicom_3D_i_array[h,:,:,:] 
    
    
    input_array_j = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_j[h][0][:][:][:] = dicom_3D_j_array[h,:,:,:]
        
    
    input_array_k = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_k[h][0][:][:][:] = dicom_3D_k_array[h,:,:,:]
    
    # Predict voxel lesion type
    CNN_3D_i_predict = CNN_3D_i.predict(input_array_i)
    CNN_3D_j_predict = CNN_3D_j.predict(input_array_j)
    CNN_3D_k_predict = CNN_3D_k.predict(input_array_k)
    
    # Compute averaging
    sigmoid_average = (CNN_3D_i_predict + CNN_3D_j_predict + CNN_3D_k_predict) / 3.0
    
    # Return the three predicted results
    return [CNN_3D_i_predict, CNN_3D_j_predict, CNN_3D_k_predict, sigmoid_average]


# Sub-routines definition
# dicom extended handler
def dicom_extended_handler():
    
    # Save arrays processed in separate list : dicom
    dicom_3D_i_array = np.array(dicom_3D_i_preprocessor())
    dicom_3D_j_array = np.array(dicom_3D_j_preprocessor())
    dicom_3D_k_array = np.array(dicom_3D_k_preprocessor())
    
    # Print the summary of all blocks in the three networks
    print(format("The summary of CNN_3D_i architecture", "*^80"))
    CNN_3D_i.summary()
    print(" ")
    print(format("The summary of CNN_3D_j architecture", "*^80"))
    CNN_3D_j.summary()
    print(" ")
    print(format("The summary of CNN_3D_j architecture", "*^80"))
    CNN_3D_j.summary()
    print(" ")
    
    # Transforming 4D into 5D for CNN Model
    input_array_i = np.ones((1, 1, 128, 128, 250))
    for h in range(1):
        input_array_i[h][0][:][:][:] = dicom_3D_i_array[h,:,:,:] 
    
    
    input_array_j = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_j[h][0][:][:][:] = dicom_3D_j_array[h,:,:,:]
        
    
    input_array_k = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_k[h][0][:][:][:] = dicom_3D_k_array[h,:,:,:]
    
    # Predict voxel lesion type
    CNN_3D_i_predict = CNN_3D_i.predict(input_array_i)
    CNN_3D_j_predict = CNN_3D_j.predict(input_array_j)
    CNN_3D_k_predict = CNN_3D_k.predict(input_array_k)
    
    # Compute averaging
    sigmoid_average = (CNN_3D_i_predict + CNN_3D_j_predict + CNN_3D_k_predict) / 3.0
    
    # Return the three predicted results
    return [CNN_3D_i_predict, CNN_3D_j_predict, CNN_3D_k_predict, sigmoid_average]


# mhd light handler
def mhd_light_handler():
    
    # Save arrays processed in separate list : mhd
    mhd_3D_i_array = np.array(mhd_3D_i_preprocessor())
    mhd_3D_j_array = np.array(mhd_3D_j_preprocessor())
    mhd_3D_k_array = np.array(mhd_3D_k_preprocessor())

    # Transforming 4D into 5D for CNN Model
    input_array_i = np.ones((1, 1, 128, 128, 250))
    for h in range(1):
        input_array_i[h][0][:][:][:] = mhd_3D_i_array[h,:,:,:] 
    
    
    input_array_j = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_j[h][0][:][:][:] = mhd_3D_j_array[h,:,:,:]
        
    
    input_array_k = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_k[h][0][:][:][:] = mhd_3D_k_array[h,:,:,:]
    
    # Predict voxel lesion type
    CNN_3D_i_predict = CNN_3D_i.predict(input_array_i)
    CNN_3D_j_predict = CNN_3D_j.predict(input_array_j)
    CNN_3D_k_predict = CNN_3D_k.predict(input_array_k)
    
    # Compute averaging
    sigmoid_average = (CNN_3D_i_predict + CNN_3D_j_predict + CNN_3D_k_predict) / 3.0
    
    # Return the three predicted results
    return [CNN_3D_i_predict, CNN_3D_j_predict, CNN_3D_k_predict, sigmoid_average]


# Sub-routines definition
# mhd extended handler
def mhd_extended_handler():
    
    # Save arrays processed in separate list : mhd
    mhd_3D_i_array = np.array(mhd_3D_i_preprocessor())
    mhd_3D_j_array = np.array(mhd_3D_j_preprocessor())
    mhd_3D_k_array = np.array(mhd_3D_k_preprocessor())
    
    # Print the summary of all blocks in the three networks
    print(format("The summary of CNN_3D_i architecture", "*^80"))
    CNN_3D_i.summary()
    print(" ")
    print(format("The summary of CNN_3D_j architecture", "*^80"))
    CNN_3D_j.summary()
    print(" ")
    print(format("The summary of CNN_3D_j architecture", "*^80"))
    CNN_3D_j.summary()
    
    
    # Transforming 4D into 5D for CNN Model
    input_array_i = np.ones((1, 1, 128, 128, 250))
    for h in range(1):
        input_array_i[h][0][:][:][:] = mhd_3D_i_array[h,:,:,:] 
    
    
    input_array_j = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_j[h][0][:][:][:] = mhd_3D_j_array[h,:,:,:]
        
    
    input_array_k = np.ones((1, 1, 128, 128, 200))
    for h in range(1):
        input_array_k[h][0][:][:][:] = mhd_3D_k_array[h,:,:,:]
    
    
    # Predict voxel lesion type
    CNN_3D_i_predict = CNN_3D_i.predict(input_array_i)
    CNN_3D_j_predict = CNN_3D_j.predict(input_array_j)
    CNN_3D_k_predict = CNN_3D_k.predict(input_array_k)
    
    # Compute averaging
    sigmoid_average = (CNN_3D_i_predict + CNN_3D_j_predict + CNN_3D_k_predict) / 3.0
    
    # Return the three predicted results
    return [CNN_3D_i_predict, CNN_3D_j_predict, CNN_3D_k_predict, sigmoid_average]

            
# Base inference engine 
def base_inference_engine():
    
    print(format("[Predicting Voxel Nodule or Non Nodule in images] Running...", "*^80"))
    
    # Sub routine controller
    if imageformat == "mhd":
        if appmode == "light":
            
            mhd_ = mhd_light_handler()
            print("3D_CNN_i: %2.2f, 3D_CNN_j: %2.2f, 3D_CNN_k: %2.2f, and Sigmoid averaging\ 
            	%2.2f" %(mhd_[0], mhd_[1], mhd_[2], mhd_[3][0][0]))
                  
            if(mhd_[3] >= 0.5):
                print("The probability of the patient having [Lung Nodule] is: %2.2f" %(mhd_[3][0][0]))
            
            else:
                
                print("The probability of the patient having [Non Lung Nodule] is %2.2f: " %(1 - mhd_[3][0][0]))
           
        
        elif appmode == "extended":
    
            mhd_ = mhd_extended_handler()
            print("3D_CNN_i predicts: %2.2f, 3D_CNN_j predicts: %2.2f, 3D_CNN_k predicts: %2.2f, and\ 
            	Sigmoid averaging: %5.2f" %(mhd_[0], mhd_[1], mhd_[2], mhd_[3]))
            
            if(mhd_[3] >= 0.5):
                
                print("The probability of the patient having [Lung Nodule] is: %2.2f" %(mhd_[3][0][0]))
            
            else:
                
                print("The probability of the patient having [Non Lung Nodule] is: %2.2f" %(1 - mhd_[3][0][0]))
           
        
    
    elif imageformat != "mhd":
        if appmode == "light":
            dicom_ = dicom_light_handler()
            print("[Predicted Probabilities] 3D_CNN_i: %2.2f, 3D_CNN_j predicts: %2.2f, 3D_CNN_k: %2.2f,\ 
            	and Sigmoid averaging: %1.2f" %(dicom_[0], dicom_[1], dicom_[2], dicom_[3][0][0]))
                  
            if(dicom_[3] >= 0.5):
                print("The probability of the patient having [Lung Nodule] is: %2.2f" %(dicom_[3][0][0]))
            
            else:
                
                print("The probability of the patient having [Non Lung Nodule] is: %2.2f" %(1 - dicom_[3][0][0]))
           
           
            
        elif appmode == "extended":
            dicom_ = dicom_extended_handler()
            print("3D_CNN_i: %2.2f, 3D_CNN_j: %2.2f, 3D_CNN_k: %2.2f, and Sigmoid averaging: %2.2f"\ 
            	%(dicom_[0], dicom_[1], dicom_[2], dicom_[3][0][0]))
                  
            if(dicom_[3] >= 0.5):
                print("The probability of the patient having [Lung Nodule] is: %2.2f" %(dicom_[3][0][0]))
            
            else:
                
                print("The probability of the patient having [Non Lung Nodule] is: %2.2f" %(1 - dicom_[3][0][0]))


# Slice viewer: CNN_3D_i
def slice_viewer_200_slices(slices_):
    
    i = 0
    # Display CT scan
    for slice_ in slices_[:1]:
        print(slice_.shape)
        for mask in slice_:
            plt.imshow(mask, cmap= "gray")
            plt.show()
            print(i)
            i = i + 1
            if i == 200:
                break

# Slice viewer: CNN_3D_j and CNN_3D_k
def slice_viewer_250_slices(slices_):
    i = 0
    # Display CT scan
    for slice_ in slices_[:1]:
        print(slice_.shape)
        for mask in slice_:
            plt.imshow(mask, cmap= "gray")
            plt.show()
            print(i)
            i = i + 1
            
            if i == 250:
                break

# Invoke the base inference handler
base_inference_engine()

print("[INFO] Exiting application")
