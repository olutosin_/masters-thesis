

## MetaImage (.mhd/.raw) & Dicom Image Processing

@ Author: Olutosin Ademola

@ Supervisor: Prof. Olev Martens


-----------------------------------------------------------------------------------------------------------------------------------
### Folders Definition
3d-scans-preprocessing: This folder holds the python code to process raw mhd/raw CT scans for all the patients files used. 

Ensemble-models-training: This folder holds three python files as named respectively. Each file holds the code for the development
and training of the 3D CNN ensemble models[3D-CNN-i, 3D-CNN-j, 3D-CNN-k] used. 

Ensemble-models-testing: This folder holds the CADeC(Computer Aided Detection and Classification) code file. It supports both 
dicom and mhd/raw files. The depth must be atleast by depth of 250 slices.  

test-folder: This folder holds all the screenshots and graphs taken during training, testing and model evalution. 

-----------------------------------------------------------------------------------------------------------------------------------
This python3 code can process thoracic computed tomography of dimension (Z * H * W) where Z, H and W vary.

### Python3.5 Enviroment Set-up

Dependencies: 

1. Keras 2.3.1 and theano 1.0.4
2. Tensorflow 2.0.0
3. Matplotlib
4. Numpy
4. OpenCV 3.4.3.18 or above
5. pydot
6. Graphivz

------------------------------------------------------------------------------------------------------------------------------------

'''
#### Load .mhd and .raw files

#### Example: To read one .mhd and .raw file (LNDb-0001.mhd and LNDb-0001.raw )

image = sitk.GetArrayFromImage(sitk.ReadImage("/path-to-.mhd/LNDb-0001.mhd"))

print(image.shape)

print(image)

"""To visualize all slices in the scan read and prints each slice shape"""

for slice in image:

   print(slice.shape)
   
   plt.imshow(slice)
   
   plt.show()
 
'''


------------------------------------------------------------------------------------------------------------------------------------

#### The work commited here (in this repository) is licensed under TalTech (Tallinn University of Technology). To use any part of this thesis, kindly contact me via: youngolutosin@gmail.com  or oladem@taltech.ee. My supervisor in person of Professor Olev Martens can also be contacted via <olev.martens@taltech.ee>. 

------------------------------------------------------------------------------------------------------------------------------------


