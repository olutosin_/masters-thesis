# File for 3D CT scan pre-processing
# Import all libraries required
import matplotlib.pyplot as plt
import SimpleITK as sitk
from pathlib import Path
import pandas as pd
import numpy as np
import warnings
import shutil
import glob
import math
import cv2
import os

print("[INFO] starting application...")

# scans directory
warnings.filterwarnings("ignore")
ScansPath = os.listdir("../Nodule1.0/ct-scans/")
files = []
for file in ScansPath:
    if file.endswith(".mhd"):
        files.append(file)
print(len(files))


# creating folders with corresponding
# names
full_path = []
parent_dir = os.listdir("../Nodule1.0/ct-scans/")
for parent in parent_dir:
    if parent.endswith(".mhd"):
        path = parent
        p = str(path[0:9])
        print(p)
        path_ = os.path.join("../Nodule1.0/scan_dir/", p)
        os.makedirs(path_)
        full_path.append(path_)
print(full_path)

# creating corresponding textfiles
# creating text files to hold paths
#  to .mhd and .raw to file
full_path_ = []
for file in os.listdir("../Nodule1.0/scan_dir/"):
    path = os.path.join("../Nodule1.0/scan_dir/", file)
    full_path_.append(path)
print(full_path_)

start_num = 1

# creating folders
for folder in full_path_:
    path = folder + "/"
    print(path)
    k = path[22:31]
    print(k)
    for file in ScansPath:
        if file.endswith(".mhd"):
            mhd_copy = file
            y = mhd_copy.split(".", 1)[0]
            if k == y:
                print(k, y)
                filename = "../Nodule1.0/scan_textpath/" + y + ".txt"
                f = open(filename, "a")
                paths = os.path.join("../Nodule1.0/ct-scans/", file)
                f.write(paths + "\n")

        if file.endswith(".raw"):
            raw_copy = file
            x = raw_copy.split(".", 1)[0]
            if k == x:
                print(k, x)
                print(file, path)
                filename = "../Nodule1.0/scan_textpath/" + x + ".txt"
                f = open(filename, "a")
                path_ = os.path.join("../Nodule1.0/ct-scans/", file)
                f.write(path_ + "\n")
                start_num += 1
print(len(os.listdir("../Nodule1.0/scan_textpath")))

# loading text_files

text_file = []
path_to_textfile = os.listdir("../Nodule1.0/scan_textpath/")
for text in path_to_textfile:
    complete_path = os.path.join("../Nodule1.0/scan_textpath/", text)
    text_file.append(complete_path)
print(text_file)
print(" ")

# load the labels
labels_df = pd.read_csv("../Nodule1.0/lndb-labels/LNDb_label.csv", index_col=0)
print(labels_df.tail())

position = []
for i, txt in enumerate(text_file):
    f = open(txt, "r")
    mhd = f.readline()
    raw = f.readline()
    path = mhd
    path = path[27:31]
    position.append(path)
print(len(position))

image3D = []
for j, i in enumerate(position[:140]):
    label = labels_df.get_value(j, 'Nodule')
    path_to_scan = "../Nodule1.0/ct-scans/LNDb-{}.mhd".format(i)
    image = sitk.GetArrayFromImage(sitk.ReadImage(os.path.abspath(path_to_scan)))
    image3D.append(image)
    print(image.shape, label, j)

# make a copy of image3D
scan3D = image3D.copy()
print(len(scan3D))

# visualizing individual slice
for i, slice in enumerate(image3D[30]):
    if (i < 180):
        print(slice.shape)
        plt.imshow(slice, cmap = "gray")
        plt.show()
    else:
         break
    

# helping functions
# @lst is a list
# @n is chunks size
def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def mean(l):
    return sum(l) / len(l)

# 3D
# @ test is the main list == patients
# for i, patient in enumerate(test)
# print(patient.shape)
# 3D list of list
w = 64
h = 64
#img_stack = 180

new_slice3D = []
length_slices_3D = []
label_main = []
roll_axis3D = [] 
new_slices3D = []

# creates a list of number 
stack = range(1, 181)
img_stack = []
for i in stack:
    img_stack.append(i)


for i, patients in enumerate(scan3D):
    print(patients.shape, i)
    
    label = labels_df.get_value(i, 'Nodule')
    label_main.append(label)
    
    new_slices = []
    for i, slice in enumerate(patients):
        if (i < 179) or (i == 179):
            img_sm = cv2.resize(np.array(slice), (w, h), interpolation=cv2.INTER_CUBIC)
            new_slices.append(img_sm)
            print("shape of image stack = {a} and i = {b}".format(a = img_sm.shape, b = i))
        
        else:
            break
  
    slices_resized_3d = np.asarray(new_slices)
    # Length of Main 3D
    length_slices_3D.append(len(slices_resized_3d))
    
    # Main 3D array
    new_slices3D.append(slices_resized_3d) 
    
    # append length of slices after each loop
    input = slices_resized_3d 
    print("shape before rolling:", np.array(new_slices3D).shape)
    
    # Rolling
    ipt = np.rollaxis(np.rollaxis(input, 2, 0), 2, 0)
    roll_axis3D.append(ipt)
    print(ipt.shape)
    print("New slice 3D:", np.array(new_slices3D).shape)
   

# saving data
data_array = np.array(new_slices3D)
np.save("50-250-128-128.npy", data_array)
label_array = np.array(label_main)
np.save("label-50-250-128-128.npy", label_array) 

print(os.listdir())

print("[INFO] Program execution status: success")



